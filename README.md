# Fork me on Codeberg CSS ribbons
For use on your [Codeberg Pages](https://pages.codeberg.org).

<details>
<summary>
See a preview
</summary>
<br>

![](screenshots/top-left-red.png)
![](screenshots/top-right-yellow.png)

![](screenshots/bottom-left-black.png)
![](screenshots/bottom-right-green.png)

</details>
<br>

## Downloading
### Manually
Download [`fork-ribbon.css`](fork-ribbon.css) to your website's assets and reference to it in the `<head>` of your page.

```html
<link rel="stylesheet" href="/assets/css/fork-ribbon.css" />
```

### npm

You can also install the CSS files using [npm](https://www.npmjs.com/).

```bash
npm install "git+https://codeberg.org/ribbons/fork-me-on.git"
```

### CDN

> Note: This method is not recommended as the URL is subject to change.

You can use the CSS files without installation via raw.codeberg.page.

Copy the following code into the `<head>` of your page:

```html
<link rel="stylesheet" href="https://raw.codeberg.page/ribbons/fork-me-on/@1.0.0/fork-ribbon.css" />
```

## Usage
Copy the following code into the `<body>` of your page, substituting your username for your Codeberg username:

```html
<a class="fork-ribbon" href="https://username.codeberg.page" data-ribbon="Fork me on Codeberg" title="Fork me on Codeberg">Fork me on Codeberg</a>
```

### Customization
#### Location
To change the location of the ribbon on the page, add one of the following classes to the ribbon code.
- right-top
- left-top
- right-bottom
- left-bottom

#### Fixed
To fix a ribbon onto the screen, add the `fixed` class to the ribbon code.

#### Color
The default color of the ribbon is `#c00`. To can change the color of the ribbon using 

## Credits
This project is based on [github-fork-ribbon-css](https://github.com/simonwhitaker/github-fork-ribbon-css/). 

## License
This project is licensed under MIT. Refer to the [LICENSE file](LICENSE).
